/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.hiranya;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 *
 * @author ambasoft
 */
@Mojo(name = "sitecopy", defaultPhase = LifecyclePhase.PRE_SITE)
public class SiteCopyMojo extends AbstractMojo {

    @Component
    private MavenProject project;

    @Parameter
    private String siteFolder;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        File source;
        File target;
        try {
            getLog().info("Copying site folder from parent...");
            if (project.isExecutionRoot()) {
                source = new File("../../" + project.getParent().getArtifactId() + "/workspace/" + siteFolder);
                //source = new File("../" + project.getParent().getArtifactId() + "/"+siteFolder);
                target = new File(siteFolder);
            } else {
                source = new File(siteFolder);
                target = new File(project.getArtifactId() + "/" + siteFolder);
            }
            getLog().info("Source:" + source.getPath());
            getLog().info("Target:" + target.getPath());
            FileUtils.copyDirectory(source, target);
            getLog().info("Site Copy completed!");
        } catch (IOException ex) {
            Logger.getLogger(SiteCopyMojo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
