/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.hiranya.art;

/**
 *
 * @author ambasoft
 */
public class Varaha {
    
    public static final String ASCII = "                                                 `-::::-`                 \n" +
"                         :+:--                `:+ooo++++o+:`                    \n" +
"                        `o/::/+-.`           `+soo++/:////++.                   \n" +
"                        `oo+/++//+-         `+sso+/:--.--:/+/`                  \n" +
"                         /+/:-::/+o-        .yss+/-...``.:/+/:                  \n" +
"                        -so+/+///+o+-.      .yysoo+/::--:/++o/                  \n" +
"           `            `+yyys/::://+o/.`    +hysso++////+oos-      `..`        \n" +
"         `.-.             -shy//:/+o+oooo//:.`/yyysooo+oooso.      `:/:-        \n" +
"    `  `.--:-`             `yso+++++:+++//+oo/``/shysssso/-`       .+:-:.``.-   \n" +
"   .------.-:.              :s++o++:/os+/::/+++/::ys//o-`      .:-::://:.-:/-   \n" +
"  .----.:::..-:.``       ./yyho+++o/-+o++//:///+ooo:/o/        `-/////::.-/:`   \n" +
"  .--:--//:--:+:--`      oshyys//+o+/+//::--///+os++os-          `.-+//:.:..`   \n" +
"   ``::::::::/+:::`      ++yyyh//-:++o+:--:/ysossssos:              `::::/+//.  \n" +
"     .://+++::-``        `/sdhs://:/syo++++syo:.....``               ::+/+o++-` \n" +
"     :://++.              `/yds+ys/+dyoooooss.                      -+/+:+/+++. \n" +
"     ://+::                `.oy++o:/so++++++++///-.``               /oo:::://+. \n" +
" .-.-+//:-.             `-/++ooo+++oo++++/+++oososs+o/-`            `ss++:///+` \n" +
" :os++/++/`    ``     .:+os////+++++///o+//o+++++os:oo+/-.          `so+oooo/`  \n" +
"  /+oy+/+/    ----.``:+ooso/::+o+//+/////:/o/+oo/:+/:/:://:.        /:-/+++o.   \n" +
"  .oyhsos-   `/-:.-:+oooso/:-:oo+++//::::/+/:/::::+//--://++/.     -+::/++o/    \n" +
"   /oo/os+`  ::.:.::ss/ys:--/ohyo++/:---o++-:---:/o+/---:/++os-``-/////o++/`    \n" +
"   .oo+osys:`/:-:-//s+/s+.-+sssoyso+/:-+/-/+:-://+++s+--:+oosyyssos+++//+o:`    \n" +
"    ./ss+/oyo+/-:/::-:+//:os-  `ohs+-.:::::/::/+o-  .+o+::/syhhdss+-+s++oo-     \n" +
"      :oo/:oss:-:-:-./o/+o+`    +yso:-:/-.-/:-/+/     :o+::+yhhhyo-/o/:-.       \n" +
"       -syo++/+/::::/+ooo:     `oso+++:::-:://+o.      .+o/osyyys/:o/-`         \n" +
"        `+yo++oo+///+oss.      `+sso+s/:::/+o+//`       `/osyyys::/-            \n" +
"          :ssss/+ooso/:.       `osysoo+::/++++/`          `/osso//`             \n" +
"           `//:.:+oo:         `+sssso/::::/+++/.            .--.`               \n" +
"                 ..`         `+ssoo+:-.:/.:/+++/`                               \n" +
"                          ```:syo+++/::::://+ooo`                               \n" +
"                         `/:oysosso++/:::+/++/o+```                             \n" +
"                     `-:/++/yso++++/++///++oosss+::                             \n" +
"                     //-./:sssso//:/+++oo/o://os/+/-.`                          \n" +
"                     :///+osso+/:/+/+++++o:o/ooo/:-.:-.                         \n" +
"                        `osso///:/oo+/+++//:::/oo::---:                         \n" +
"                        .+so+//://+oy+oo++/:::/+o/-..`                          \n" +
"                        -+ss+//:/:+so/+so+:-:-/++/-                             \n" +
"                        .+so+/+//:+oo/:oo+::/-:/+:-                             \n" +
"                         +so++/://+oo/-+s/:.:::/+::`                            \n" +
"                         ::so+::::+oo+//oo+/:-:/+:/                             \n" +
"                         -/os+/...:o++/:oo+/:--:+//                             \n" +
"                         `++ys+::-/oo+/:ss++-..:++:                             \n" +
"                          :/yso/::+++//:+soo+:-/+//                             \n" +
"                          ./+so++/+/+/--:soo+/:/++:                             \n" +
"                          `//so+/:::/+--:oo+///:++:                             \n" +
"                           +:ss+:---/+:-:+o+/:--++/                             \n" +
"                           /:+s+/:--/o:-:/oo:--:+//`                            \n" +
"                           /-/so+/:::+/-:/oo/--:+:/.                            \n" +
"                           /--+oo//:://://o+/::/++/-                            \n" +
"                           ::-+o+/:://:../s+/://+/::.                           \n" +
"                          `:/::/o+:/o.    -s+/+o-```                            \n" +
"                           ``  `+/:/+`    .s+-:/`                               \n" +
"                               `/+::+.    `o+::+.                               \n" +
"                               :++//+-    `syooo/:.                             \n" +
"                              -///:/::    .syhhs+o+/:                           \n" +
"                            `-+//://::::::+ossy+/ssss.                          \n" +
"                            /ooo+++/:-..-.-..-:/+osss+                          \n" +
"                            :oso+++/:-..---.-:/+oooss/                          \n" +
"                            `+sssooo+::/+//:--::/+oo/`                          \n" +
"                             :oso/oo+:.-..```.-:++o+:                           \n" +
"                             -oso+++/:::.````--:/+o+.                           \n" +
"                            `:oooo++///::----::/++++-`                          \n" +
"                          `./ooooo+/::------.-:/+++++:.`                        \n" +
"                        .-/++ooossssoooo++++++ooooo++//:-.`                     \n" +
"                      `:/++ooosooooo++++++++++++++++++++//-.`                   \n" +
"                     ./+++ossoo+++/////////////////+++++++/:-`                  \n" +
"                    .//++osooo++/:::::::::::::::::///+/+ooo+/:.                 \n" +
"                   `//+oossoo+//-----.------.....--::///++o++/:`                \n" +
"                   -+ooooo+++//:---......-..``````.--::://++++/-                \n" +
"                  -:/+oooo++///:--:........``````..---::///++///-`              \n" +
"                 `/osssooo++++/////::::::/::::::::::://++++ooss+/`              \n" +
"                 `/+oosoooo+///:--.``````...``  ``...-://++ooo+/:`              \n" +
"                  .-:://////:/::::-......--..````....--::::::::-.               ";
}
