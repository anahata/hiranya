/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.hiranya;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import uno.anahata.hiranya.art.Varaha;

/**
 *
 * @author ambasoft
 */
@Mojo(name = "rundoc", defaultPhase = LifecyclePhase.SITE)
public class HiranyaMojo extends AbstractMojo {

    private static final String ADWORD_DIV = "adWordsDiv";

    private static final String HEADER_DIV = "siteHeaderDiv";

    private static final String FOOTER_DIV = "siteFooterDiv";

    private static final String ANAHATA_DIV = "anahataLinkDiv";

    private static final String PAYPAL_DIV = "paypalLinkDiv";

    private static final String ADWORD_DIV_POS = "header";

    private static final String ANAHATA_DIV_POS = "details";

    @Component
    private MavenProject project;

    @Parameter
    private String javaDocFolder;

    @Parameter
    private String adWordsScript;

    @Parameter
    private String donateWithPaypal;

    @Parameter
    private List<BackLink> backLinks;

    @Parameter
    private String headerContent;

    @Parameter
    private String footerContent;

    private static final String UTF_8 = "UTF-8";

    private final List<String> fileList;

    public HiranyaMojo() {
        fileList = new ArrayList<>();
    }

    public List<String> getFileList() {
        return fileList;
    }

    public void includeAdWordsToJavaDocContent(String fileName, String adContent, String locationClass) throws
            IOException {
        Document doc = Jsoup.parse(new File(fileName), UTF_8);
        Elements elements = doc.getElementsByClass(locationClass);
        for (Element element : elements) {
            if (!isLinkExists(element, ADWORD_DIV)) {
                element.prepend(adContent);
            }
        }
        replaceFile(fileName, doc);
    }

//    public void includeHeaderToMavenSite(String fileName, String headerContent, String locationClass) 
//            throws IOException {
//        final Document doc = Jsoup.parse(new File(fileName), UTF_8);
//        final Elements headElements = doc.getElementsByTag("head");
//        for(Element headElement: headElements){
//            if(!isScriptExists(headElement, "csi")){
//                if(!fileName.contains("apidocs")) {
//                    headElement.append("<script src='js/csi.min.js'></script>");
//                }
//            }
//        }
//        final Elements elements = doc.getElementsByTag("body");
//        for (Element element : elements) {
//            if (!isLinkExists(element, HEADER_DIV)) {
//                element.prepend(headerContent);
//            }
//        }
//        replaceFile(fileName, doc);
//    }
//
//    public void includeFooterToMavenSite(String fileName, String footerContent, String locationClass) 
//            throws IOException {
//        Document doc = Jsoup.parse(new File(fileName), UTF_8);
//        Elements elements = doc.getElementsByTag("body");
//        for (Element element : elements) {
//            if (!isLinkExists(element, FOOTER_DIV)) {
//                element.append(footerContent);
//            }
//        }
//        replaceFile(fileName, doc);
//    }


    public void includePaypalDonateToJavaDocContent(String fileName, String adContent, String locationClass) throws
            IOException {
        Document doc = Jsoup.parse(new File(fileName), UTF_8);
        Elements elements = doc.getElementsByClass(locationClass);
        for (Element element : elements) {
            if (!isLinkExists(element, PAYPAL_DIV)) {
                element.prepend(adContent);
            }
        }
        replaceFile(fileName, doc);
    }

    public void includeSiteLinkToJavaDocContent(String fileName, String adContent, String locationClass) throws
            IOException {
        Document doc = Jsoup.parse(new File(fileName), UTF_8);
        Elements elements = doc.getElementsByClass(locationClass);
        for (Element element : elements) {
            if (!isLinkExists(element, ANAHATA_DIV)) {
                element.append(adContent);
            }
        }
        replaceFile(fileName, doc);
    }

    public void cleanupJavaDocContent(String fileName, String elementClass) throws IOException {
        Document doc = Jsoup.parse(new File(fileName), UTF_8);
        Elements elements = doc.getElementsByClass(elementClass);
        for (Element element : elements) {
            element.remove();
        }
        replaceFile(fileName, doc);
    }

    public void replaceFile(String fileName, Document doc) throws IOException {
        try (final BufferedWriter htmlWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName),
                UTF_8))) {
            String output = doc.toString();
            htmlWriter.write(output);
        }
    }

    public boolean isLinkExists(Element element, String adContent) {
        return (element.text().contains(adContent));
    }

    public boolean isScriptExists(Element element, String scriptContent) {
        return (element.text().contains(scriptContent));
    }

    public void populateFiles(String path) {
        File file = new File(path);
        String[] content = file.list();
        if (null != content) {
            for (String fileName : content) {
                File innerFile = new File(path + "/" + fileName);
                if (innerFile.getAbsolutePath().endsWith(".html")) {
                    fileList.add(innerFile.getAbsolutePath());
                }
                if (innerFile.isDirectory()) {
                    populateFiles(path + "/" + fileName);
                }
            }
        }
    }

    public String getBackLinksFromList() {
        final StringBuilder linkBuilder = new StringBuilder("<div class='" + ANAHATA_DIV + "'> | ");
        for (BackLink backLink : backLinks) {
            linkBuilder.append("<a href='");
            linkBuilder.append(backLink.href);
            linkBuilder.append("'>");
            linkBuilder.append(backLink.anchorText);
            linkBuilder.append("</a> ");
        }
        linkBuilder.append("| </div>");
        return linkBuilder.toString();
    }

    public String getAdWordsDiv() {
        final StringBuilder divBuilder = new StringBuilder("<div class='" + ADWORD_DIV + "'>");
        divBuilder.append(adWordsScript);
        divBuilder.append("</div>");
        return divBuilder.toString();
    }

    public String getPaypalDiv() {
        final StringBuilder divBuilder = new StringBuilder("<div class='" + PAYPAL_DIV + "'>");
        divBuilder.append(donateWithPaypal);
        divBuilder.append("</div>");
        return divBuilder.toString();
    }

    public String getSiteHeaderDiv() {
        final StringBuilder divBuilder = new StringBuilder("<div class='" + HEADER_DIV + "'>");
        divBuilder.append("<div data-include='");
        divBuilder.append(headerContent);
        divBuilder.append("'></div>");
        divBuilder.append("</div><br/>");
        return divBuilder.toString();
    }

    public String getSiteFooterDiv() {
        final StringBuilder divBuilder = new StringBuilder("<div class='" + FOOTER_DIV + "'>");
        divBuilder.append("<div data-include='");
        divBuilder.append(footerContent);
        divBuilder.append("'></div>");
        divBuilder.append("</div><br/>");
        return divBuilder.toString();
    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        System.out.println("*************** Hiranya Plugin Starts **********");
        getLog().info("Starting the Javadoc updation process...");

        populateFiles(project.getBuild().getDirectory() + javaDocFolder);
        for (String file : getFileList()) {
            try {
                cleanupJavaDocContent(file, ADWORD_DIV);
                cleanupJavaDocContent(file, ANAHATA_DIV);
                cleanupJavaDocContent(file, PAYPAL_DIV);
                includeAdWordsToJavaDocContent(file, getAdWordsDiv(), ADWORD_DIV_POS);
                includePaypalDonateToJavaDocContent(file, getPaypalDiv(), ADWORD_DIV_POS);
                includeSiteLinkToJavaDocContent(file, getBackLinksFromList(), ANAHATA_DIV_POS);
                //includeHeaderToMavenSite(file, getSiteHeaderDiv(), "pull-left");
                //includeFooterToMavenSite(file, getSiteFooterDiv(), "span10");
            } catch (Exception ex) {
                Logger.getLogger(HiranyaMojo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //http://www.text-image.com/convert/pic2ascii.cgi
        System.out.println(Varaha.ASCII);
        getLog().info("Javadoc updates completed!");
        System.out.println("*************** Hiranya Plugin Ends **********");
    }

}
